const bodyParser = require("body-parser");
require('body-parser-xml')(bodyParser);

const crypto = require("crypto");
const CryptoJS = require("crypto-js");
const axios = require('axios');

const { appid : olpayAppid, userid, key, privK, certID, v3key, platform_public_key } = require('../../global').olpay;
const { wx } = require('../../global');

const appid = (mp) => (mp && wx[mp] && wx[mp].appid) || (wx && wx.appid) || olpayAppid;

const wxPayAgent = axios.create({
    baseURL: 'https://api.mch.weixin.qq.com',
    timeout: 30 * 1000,
    headers: {
        'Content-Type':'application/json;charset=utf-8',
        'Accept':'application/json',
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
    }
});

const POST_PROCESS_CACHE = {};

const nonce = (l = 32) => {
    const chars = 'ABCDEFGHJKMNPQRSTWXYZ1234567890';
    let nonce_str = '';
    for (let i = l; i > 0; --i) {
        nonce_str += chars[Math.floor(Math.random() * chars.length)];
    }

    return nonce_str;
}

const sign = (obj) => {
    if (!obj) return '';

    let stringSignArray = [];

    const keys = Object.keys(obj).sort((a, b) => a.localeCompare(b));
    for (let i = 0; i < keys.length; i += 1) {
        const k = keys[i];

        if (k && !!obj[k]) {
            stringSignArray.push(`${k}=${obj[k]}`);
        }
    }

    stringSignArray.push(`key=${key}`);

    const stringSignTemp = stringSignArray.join('&');
    const signStr = CryptoJS.MD5(stringSignTemp).toString();

    return (signStr || '').toUpperCase();
}

const xmlBuilder = (obj = {}, options = {}) => {
    obj = obj || {};

    const { ignoreEmpty, useCData, expand } = options;
    let xmlStr = '';

    const keys = Object.keys(obj);

    for (let i = 0; i < keys.length; i += 1) {
        const k = keys[i];

        if (!obj[k] && ignoreEmpty) continue;

        let nodeStr = '';

        if (typeof obj[k] === 'object' && !Array.isArray(obj[k])) {
            // nest
            if (expand) {
                nodeStr = `${xmlBuilder(obj[k], options)}`;
            } else {
                nodeStr = `${useCData ? '<![CDATA[' : ''}${JSON.stringify(obj[k])}${useCData ? ']]>' : ''}`;
            }
        } else if (Array.isArray(obj[k])) {
            nodeStr = `[${obj[k].toString()}]`;
        } else {
            nodeStr = `${obj[k].toString()}`;
        }

        xmlStr += `<${k}>${nodeStr || ''}</${k}>`
    }

    return xmlStr;
}

const xmlParser = (xml = '') => {
    if (!xml || !xml.startsWith('<') || !xml.endsWith('>')) {
        return {};
    }

    xml = xml.replace(/\\n/g, '').replace(/\s/g, '').replace(/\n/g, '');
    const ret = {};

    let leftStr = xml;
    while (leftStr.length > 0) {
        let tag = leftStr.substring(0, leftStr.indexOf('>') + 1);
        tag = tag.substring(1);
        tag = tag.substring(0, tag.length - 1);

        if (!tag) break;

        let v = leftStr.substring(`<${tag}>`.length, leftStr.indexOf(`</${tag}>`));

        if (v.startsWith('<![CDATA[')) {
            v = v.substring('<![CDATA['.length, v.length - ']]>'.length);
        } else if (v.startsWith('<')) {
            v = xmlParser(v);
        }

        ret[tag] = v;

        leftStr = leftStr.substring(leftStr.indexOf(`</${tag}>`) + `</${tag}>`.length)

        if (!leftStr.startsWith('<')) {
            break;
        }
    }

    return ret;
}

const DBOperation = (app, list) => {
    if (!app || !list || typeof list !== 'object') return;

    if (!Array.isArray(list)) {
        list = [list];
    }

    for (let i = 0; i < list.length; i += 1) {
        const op = list[i];

        if (!op.collection || !op.method) continue;

        switch (op.method) {
            case 'C':
                // create new record
                app.models[op.collection].create(op.obj);
                break;
            case 'U':
                // update existing records
                app.models[op.collection].updateMany(op.filter || {}, op.obj)
                break;
            case 'D':
                // delete existing records
                app.models[op.collection].deleteMany(op.filter || {})
                break;
            default:
                break;
        }
    }
}

const wxPayV3 = {
    LAST_TS_GETTING_PLATFORM_CERTS: 0,
    PLATFORM_CERTS: [],
    AUTH_TAG_LENGTH: 16,
    sign: (content, pk) => {
        if (!content || !content.length) return '';

        const signBody = [
            ...content,
            '',
        ];

        const signer = crypto.createSign("RSA-SHA256");
        signer.update(signBody.join('\n'));
        return signer.sign(pk || privK, "base64");
    },
    wxPubKey: (signature_serial) => {
        let pubK;
        let serial = signature_serial;
        if (signature_serial) {
            const theCert = wxPayV3.PLATFORM_CERTS.find((cert) => cert.serial_no === signature_serial);

            if (theCert) {
                pubK = theCert.pubK;
            }
        } else {
            pubK = wxPayV3.PLATFORM_CERTS[0].pubK;
            serial = wxPayV3.PLATFORM_CERTS[0].serial_no;
        }

        pubK = pubK || platform_public_key;

        return {
            key: pubK,
            serial_no: serial,
        };
    },
    verify: (content, signature, signature_serial, signature_format = 'base64') => {
        const verifyer = crypto.createVerify('RSA-SHA256');
        verifyer.update([...content, ''].join('\n'))

        let pubK = wxPayV3.wxPubKey(signature_serial).key;

        return pubK ? verifyer.verify(pubK, signature, signature_format) : false;
    },
    encrypt: (str, inputEncoding = 'base64', outputEncoding = 'base64') => {
        let pubK = wxPayV3.wxPubKey();
        const encryptText = crypto.publicEncrypt({
            key: pubK.key,
            padding: crypto.constants.RSA_PKCS1_OAEP_PADDING,
        }, Buffer.from(str, inputEncoding));

        return {
            ciphertext: encryptText.toString(outputEncoding),
            signature_serial: pubK.serial_no,
        }
    },
    decrypt: (ciphertext, nonce, aad) => {
        const AAD = Buffer.from(aad);
        let cipherBuffer = Buffer.from(ciphertext, 'base64');
        const authTag = cipherBuffer.slice(-wxPayV3.AUTH_TAG_LENGTH);
        cipherBuffer = cipherBuffer.slice(0, cipherBuffer.length - wxPayV3.AUTH_TAG_LENGTH);

        const decipher = crypto.createDecipheriv('aes-256-gcm',v3key, nonce, {
        authTagLength: wxPayV3.AUTH_TAG_LENGTH
        });
        decipher.setAuthTag(authTag);
        decipher.setAAD(AAD);
        const msg = decipher.update(cipherBuffer);
        const finData = decipher.final();

        return new TextDecoder('utf8').decode(Buffer.concat([msg, finData]));
    },
    verifyPayCallback: (req) => {
        const body = req.body;
        const headers = req.headers;
      
        return wxPayV3.verify([
            headers['wechatpay-timestamp'],
            headers['wechatpay-nonce'],
            JSON.stringify(body).replace(/\n/g, ''),
          ], headers['wechatpay-signature'], headers['wechatpay-serial']);      
    },
    getCerts: async (force = false) => {
        // should we get the new certs?
        if (!force && wxPayV3.PLATFORM_CERTS.length && (Date.now() - wxPayV3.LAST_TS_GETTING_PLATFORM_CERTS) < 10 * 3600 * 1000) {
            return wxPayV3.PLATFORM_CERTS;
        }

        // get new
        const nonce_str = nonce();
        const ts = `${(Date.now() / 1000).toFixed(0)}`;

        const signStr = wxPayV3.sign(['GET', '/v3/certificates', ts, nonce_str, '']);

        const objWithSign = {
            mchid: userid,
            serial_no: certID,
            nonce_str,
            timestamp: ts,
            signature: signStr
        };
        let stringSignArray = [];

        const keys = Object.keys(objWithSign).sort((a, b) => a.localeCompare(b));
        for (let i = 0; i < keys.length; i += 1) {
            const k = keys[i];

            if (k && !!objWithSign[k]) {
                stringSignArray.push(`${k}="${objWithSign[k]}"`);
            }
        }

        const stringSignTemp = stringSignArray.join(',');

        let wxResult;
        try {
            wxResult = await wxPayAgent.get('/v3/certificates', {
                headers: {
                    Authorization: `WECHATPAY2-SHA256-RSA2048 ${stringSignTemp}` 
                }
            });
        } catch(err) {
            return wxPayV3.PLATFORM_CERTS || [];
        }

        wxResult = wxResult && wxResult.data && wxResult.data.data;
        if (Array.isArray(wxResult)) {
            wxPayV3.PLATFORM_CERTS = wxResult.sort((a, b) => new Date(a.effective_time) - new Date(b.effective_time));

            // decrypt the public key
            for (let i = 0; i < wxPayV3.PLATFORM_CERTS.length; i += 1) {
                const cert = wxPayV3.PLATFORM_CERTS[i];

                if (!cert.pubK) {
                    const decryptedK = wxPayV3.decrypt(cert.encrypt_certificate.ciphertext, cert.encrypt_certificate.nonce, cert.encrypt_certificate.associated_data);
                    const pubK = crypto.createPublicKey(decryptedK).export({type: 'spki', format: 'pem'});
    
                    if (pubK) {
                        cert.pubK = pubK;
                    }
                }
            }

            wxPayV3.LAST_TS_GETTING_PLATFORM_CERTS = Date.now();
        }

        return wxPayV3.PLATFORM_CERTS;
    },
    preorder: async (mp, data, app) => {
        const requestObj = {
            appid: appid(mp),
            mchid: userid,
            description: data.ProductsDescription || '',
            out_trade_no: data.Identity,
            notify_url: data.NotifyUrl || `${app.modules.olpay.config.hostName}${app.config['baseUrl'] || ''}/wxpay/callback`,
            amount: {
                total: data.Amount || 0,
                currency: data.Currency || 'CNY',
            },
            payer: {
                openid: data.openid,
            },
        };

        const nonce_str = nonce();
        const ts = `${(Date.now() / 1000).toFixed(0)}`;
        const rBodyStr = JSON.stringify(requestObj).replace(/\n/g, '');

        const signStr = wxPayV3.sign(['POST', '/v3/pay/transactions/jsapi', ts, nonce_str, rBodyStr]);

        const objWithSign = {
            mchid: userid,
            serial_no: certID,
            nonce_str,
            timestamp: ts,
            signature: signStr
        };
        let stringSignArray = [];

        const keys = Object.keys(objWithSign).sort((a, b) => a.localeCompare(b));
        for (let i = 0; i < keys.length; i += 1) {
            const k = keys[i];

            if (k && !!objWithSign[k]) {
                stringSignArray.push(`${k}="${objWithSign[k]}"`);
            }
        }

        const stringSignTemp = stringSignArray.join(',');

        let wxResult;
        try {
            wxResult = await wxPayAgent.post('/v3/pay/transactions/jsapi', requestObj, {
                headers: {
                    Authorization: `WECHATPAY2-SHA256-RSA2048 ${stringSignTemp}` 
                }
            });
        } catch(err) {
            //
        }

        return wxResult;
    },
    preorder_sign: (mp, ts, nonce, prepay_id) => {
        return wxPayV3.sign([
            appid(mp),
            ts,
            nonce,
            `prepay_id=${prepay_id}`,
        ]);
    },
    widthdraw: async (mp, data) => {
        const requestObj = {
            appid: appid(mp),
            out_batch_no: data.Identity,
            batch_name: data.Title,
            batch_remark: data.Title,
            total_amount: data.Amount,
            total_num: 1,
            transfer_detail_list: [
              {
                out_detail_no: data.Identity,
                transfer_amount: data.Amount,
                transfer_remark: data.Title,
                openid: data.WxOpenId,
              }
            ],
        };

        const headers = {};
        if (data.Name) {
            const cipherD = wxPayV3.encrypt(data.Name);
            data.transfer_detail_list[0].user_name = cipherD.ciphertext;
            headers['Wechatpay-Serial'] = cipherD.signature_serial;
        }

        const nonce_str = nonce();
        const ts = `${(Date.now() / 1000).toFixed(0)}`;
        const rBodyStr = JSON.stringify(requestObj).replace(/\n/g, '');

        const signStr = wxPayV3.sign(['POST', '/v3/transfer/batches', ts, nonce_str, rBodyStr]);

        const objWithSign = {
            mchid: userid,
            serial_no: certID,
            nonce_str,
            timestamp: ts,
            signature: signStr
        };
        let stringSignArray = [];

        const keys = Object.keys(objWithSign).sort((a, b) => a.localeCompare(b));
        for (let i = 0; i < keys.length; i += 1) {
            const k = keys[i];

            if (k && !!objWithSign[k]) {
                stringSignArray.push(`${k}="${objWithSign[k]}"`);
            }
        }

        const stringSignTemp = stringSignArray.join(',');

        let wxResult;
        try {
            wxResult = await wxPayAgent.post('/v3/transfer/batches', requestObj, {
                headers: {
                    Authorization: `WECHATPAY2-SHA256-RSA2048 ${stringSignTemp}`,
                    ...headers,
                }
            });
        } catch(err) {
            wxResult = err.response;
        }

        return wxResult;
    },
};

module.exports = (app) => ({
    config: {
        asRouteService: false,
        hostName: 'https://pay.xixineis.com',
    },
    data: {
        wxpay: {
            // 唯一标识
            Identity: { type: 'String', required: true, index: true, unique: true },

            // 发起支付的用户ID
            User: { type: 'String', required: true, index: true },

            // 商品详情列表
            Products: { type: 'Array', default: [] },

            // 商品描述
            ProductsDescription: { type: 'String', default: '' },

            // 附加信息
            ExtraInfo: { type: 'String' },

            // 二维码地址
            QRCode: { type: 'String', required: true, index: true, unique: true },

            // 支付状态, [undefined 未支付, '1' 支付成功, '-1' 支付失败]
            Status: { type: 'String', index: true },

            // 支付金额
            Amount: { type: 'String' },

            // 订单详细信息
            Info: { type: 'Object', default: {} },

            Success: { type: 'Object' },
            Fail: { type: 'Object' },

            // 回调传递的参数
            CBData: { type: 'Object' },
        },
    },
    wx: {
        v3: wxPayV3,
        nonce,
        order: async (mp, data /** 用于生成订单的数据 */, success/** 成功时的回调 */, fail /** 失败时的回调 */) => {
            if (!data || !data.User || !data.Identity) return;

            POST_PROCESS_CACHE[data.Identity] = {};

            if (typeof success === 'function') {
                POST_PROCESS_CACHE[data.Identity].success = success;
            }
            if (typeof fail === 'function') {
                POST_PROCESS_CACHE[data.Identity].fail = fail;
            }

            // 使用分作为单位
            data.Amount = (data.Amount || 0) * 100;

            const requestObj = {
                appid: appid(mp),
                body: data.ProductsDescription || '',
                mch_id: userid,
                nonce_str: nonce(),
                notify_url: `${app.modules.olpay.config.hostName}${app.config['baseUrl'] || ''}/wxpay/callback`,
                out_trade_no: data.Identity,
                spbill_create_ip: '111.198.247.84',
                total_fee: data.Amount,
                trade_type: 'NATIVE',
            };

            const requestXML = `<xml>${xmlBuilder({
                ...requestObj,
                sign: sign(requestObj),
            }, { useCData: true, ignoreEmpty: true })}</xml>`;


            let wxResult = await wxPayAgent.post('/pay/unifiedorder', requestXML)

            if (!wxResult || wxResult.status !== 200 || wxResult.statusText !== 'OK') {
                // 下单失败
                return; // (wxResult && wxResult.return_msg) || undefined;
            }

            wxResult = wxResult.data;
            wxResult = xmlParser(wxResult).xml;

            if (!wxResult || wxResult.return_code !== 'SUCCESS' || wxResult.result_code !== 'SUCCESS' || !wxResult.code_url) {
                // 下单失败
                return; // (wxResult && wxResult.return_msg) || undefined;
            }

            const order = data;
            order.QRCode = wxResult.code_url;

            if (typeof success === 'object') {
                data.Success = success;
            }
            if (typeof fail === 'object') {
                data.Fail = fail;
            }

            await app.models.wxpay.create(order);

            return wxResult.code_url || '';
        },
        check: async (mp, url) => {
            if(!url) return;

            const theOrder = await app.models.wxpay.findOne({QRCode: url});
            if(!theOrder) return;

            if(typeof theOrder.Status === 'undefined') {
                // 未回调，查单
                const requestObj = {
                    appid: appid(mp),
                    mch_id: userid,
                    nonce_str: nonce(),
                    out_trade_no: theOrder.Identity,
                };

                const requestXML = `<xml>${xmlBuilder({
                    ...requestObj,
                    sign: sign(requestObj),
                }, { useCData: true, ignoreEmpty: true })}</xml>`;

                let wxResult = await wxPayAgent.post('/pay/orderquery', requestXML)
                wxResult = wxResult.data;
                wxResult = xmlParser(wxResult).xml;

                switch(wxResult.trade_state) {
                    case 'SUCCESS': //--支付成功
                        theOrder.Status = '1';

                        break;
                    case 'REFUND': //--转入退款
                        break;
                    case 'NOTPAY': //--未支付
                        break;
                    case 'CLOSED': //--已关闭
                        break;
                    case 'REVOKED': //--已撤销(刷卡支付)
                        theOrder.Status = '-1';

                        break;
                    case 'USERPAYING': //--用户支付中
                        break;
                    case 'PAYERROR': //--支付失败(其他原因，如银行返回失败)
                        theOrder.Status = '-1';

                        break;
                    case 'ACCEPT': //--已接收，等待扣款
                        break;
                    default:
                        break;
                }

                if (theOrder.Status === '1') {
                    if (theOrder.Success) {
                        DBOperation(app, theOrder.Success);
                    } else if (POST_PROCESS_CACHE[theOrder.Identity].success) {
                        POST_PROCESS_CACHE[theOrder.Identity].success();
                    }

                    // 更新订单数据
                    theOrder.CBData = wxResult;
                    theOrder.save();
                } else if (theOrder.Status === '-1'){
                    if (theOrder.Fail) {
                        DBOperation(app, theOrder.Fail);
                    } else if (POST_PROCESS_CACHE[theOrder.Identity].fail) {
                        POST_PROCESS_CACHE[theOrder.Identity].fail();
                    }

                    // 更新订单数据
                    theOrder.CBData = wxResult;
                    theOrder.save();
                }
            }

            return {
                s: theOrder.Status
            }
        },
    },
    hooks: {
        onRoutersReady: async (app) => {
            app.post(`${app.config['baseUrl'] || ''}/wxpay/callback`,
                bodyParser.xml({
                    limit: '2MB',
                    xmlParseOptions: {
                        normalize: true,     // Trim whitespace inside text nodes
                        normalizeTags: true, // Transform tags to lowercase
                        explicitArray: false // Only put nodes in array if >1
                    }
                }),
                async (req, res, next) => {
                    if (!req.body || !req.body.xml) {
                        res.locals.data = `<xml>${xmlBuilder({
                            return_code: 'FAIL',
                            return_msg: '参数格式校验错误',
                        }, { useCData: true, ignoreEmpty: true })}</xml>`;
                        return next();
                    }

                    const data = req.body.xml;
                    // const data = {
                    //     appid: "wx09d24641d103b0f4",
                    //     bank_type: "OTHERS",
                    //     cash_fee: "1",
                    //     fee_type: "CNY",
                    //     is_subscribe: "N",
                    //     mch_id: "1610526332",
                    //     nonce_str: "H8Y622HX4WK3N1F1CMGSAMMF0K7JM3YM",
                    //     openid: "o9MPT5rop6FbE17DQovNuUgyC7jU",
                    //     out_trade_no: "pp5ff567cbd213e384da286d3d383295",
                    //     result_code: "SUCCESS",
                    //     return_code: "SUCCESS",
                    //     sign: "9F94D38431FA760667C8C8C6EA88F517",
                    //     time_end: "20210610103310",
                    //     total_fee: "1",
                    //     trade_type: "NATIVE",
                    //     transaction_id: "4200001155202106101511260646",
                    // }

                    if (data.appid !== appid('video') && data.appid !== appid('image')) {
                        res.locals.data = `<xml>${xmlBuilder({
                            return_code: 'FAIL',
                            return_msg: '参数格式校验错误',
                        }, { useCData: true, ignoreEmpty: true })}</xml>`;
                        return next();
                    }

                    //验证签名
                    const theSign = data.sign;
                    delete data.sign;
                    if (sign(data) !== theSign) {
                        res.locals.data = `<xml>${xmlBuilder({
                            return_code: 'FAIL',
                            return_msg: '签名失败',
                        }, { useCData: true, ignoreEmpty: true })}</xml>`;
                        return next();
                    }

                    // 查询已保存的支付订单
                    const theOrder = await app.models.wxpay.findOne({ Identity: data.out_trade_no });
                    if (theOrder && typeof theOrder.Status !== 'undefined') {
                        // 已经处理过
                        res.locals.data = `<xml>${xmlBuilder({
                            return_code: 'FAIL',
                            return_msg: 'OK',
                        }, { useCData: true, ignoreEmpty: true })}</xml>`;
                        return next();
                    }

                    if (!theOrder || `${theOrder.Amount}` !== data.total_fee) {
                        res.locals.data = `<xml>${xmlBuilder({
                            return_code: 'FAIL',
                            return_msg: '参数格式校验错误',
                        }, { useCData: true, ignoreEmpty: true })}</xml>`;
                        return next();
                    }

                    // 支付失败
                    if (data.result_code !== 'SUCCESS') {
                        theOrder.Status = '-1';

                        if (theOrder.Fail) {
                            DBOperation(app, theOrder.Fail);
                        } else if (POST_PROCESS_CACHE[data.out_trade_no].fail) {
                            POST_PROCESS_CACHE[data.out_trade_no].fail();
                        }
                    } else {
                        theOrder.Status = '1';

                        if (theOrder.Success) {
                            DBOperation(app, theOrder.Success);
                        } else if (POST_PROCESS_CACHE[data.out_trade_no].success) {
                            POST_PROCESS_CACHE[data.out_trade_no].success();
                        }
                    }

                    // 更新订单数据
                    theOrder.CBData = data;
                    theOrder.save();

                    res.locals.data = `<xml>${xmlBuilder({
                        return_code: 'FAIL',
                        return_msg: 'OK',
                    }, { useCData: true, ignoreEmpty: true })}</xml>`;

                    // TODO: 关闭订单？？

                    return next();
                },
            );
        }
    }
})